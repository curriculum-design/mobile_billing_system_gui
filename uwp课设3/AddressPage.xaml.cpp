﻿//
// AddressPage.xaml.cpp
// AddressPage 类的实现
//

#include "pch.h"
#include "AddressPage.xaml.h"
#include "data.h"
#include "help.h"
#include <vector>
#include "contactPage.xaml.h"
#include "ciPage.xaml.h"
using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

AddressPage::AddressPage()
{
	InitializeComponent();
}

namespace _AddressPage
{
	ItemCollection^ lvi;//纵向堆栈
	String^ searchname;
	int i;
}

void appendItem(String ^content)//用来添加条目的函数
{
	auto listitem = ref new ListViewItem();//创建一行条目，指针为listitem
	listitem->Content = content;//令列表内容等于content
	_AddressPage::lvi->Append(listitem);
}

void uwp课设3::AddressPage::ListView_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//进入通讯录界面触发该事件
{
	_AddressPage::lvi = ((ListView^)sender)->Items;
	_AddressPage::lvi->Clear();
	for (int i = 0; i < data::info->printaddress().size(); i++)//循环通讯录联系人个数次
	{
		appendItem(help::toPlaStr(data::info->printaddname(i)));
	}
	//添加条目:用for循环添加
}

int _contactPage::selectSub;//add的下标

void uwp课设3::AddressPage::ListView_SelectionChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::SelectionChangedEventArgs^ e)//点击条目中的内容
{
	auto lv = (ListView^)sender;
	auto item = (ListViewItem^)lv->SelectedItem;
	String^ content = (String^)(item->Content);//content为点击的框中内容，通过content到address中检索，在新页面显示联系人的信息
	_contactPage::selectSub = data::info->searchadd(help::toStdStr(content));//把selectSub通过searchadd函数赋值成add的下标，在contactPage中使用
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(contactPage::typeid));

}


void uwp课设3::AddressPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//返回ciPage
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(ciPage::typeid));

}





void uwp课设3::AddressPage::TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)//搜索框
{
	TextBox^ tb = (TextBox^)sender;
	_AddressPage::searchname = tb->Text;//_AddressPage::searchname为搜索内容
	_contactPage::selectSub = data::info->searchadd(help::toStdStr(_AddressPage::searchname));//修改_contactPage::selectSub（add的下标值，用来初始化contactpage）值

}


void uwp课设3::AddressPage::Button_Click_2(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(contactPage::typeid));//跳转contactPage页
}
