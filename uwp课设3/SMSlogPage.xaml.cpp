﻿//
// SMSlogPage.xaml.cpp
// SMSlogPage 类的实现
//

#include "pch.h"
#include "SMSlogPage.xaml.h"
#include "data.h"
#include "help.h"
#include "ciPage.xaml.h"
using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

SMSlogPage::SMSlogPage()
{
	InitializeComponent();
}


void uwp课设3::SMSlogPage::TextBlock_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->smslogprint());
}


void uwp课设3::SMSlogPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//跳转
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(ciPage::typeid));
}
