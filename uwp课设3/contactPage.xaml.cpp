﻿//
// contactPage.xaml.cpp
// contactPage 类的实现
//

#include "pch.h"
#include "contactPage.xaml.h"
#include "help.h"
#include "data.h"
#include "AddressPage.xaml.h"


using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

contactPage::contactPage()
{
	InitializeComponent();
}

#define i _contactPage::selectSub

void uwp课设3::contactPage::TextBlock_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printaddname(i));//显示name
}


void uwp课设3::contactPage::TextBlock_Loaded_1(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printaddunit(i));//显示unit
}


void uwp课设3::contactPage::TextBlock_Loaded_2(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printaddnum(i));//显示num

}


void uwp课设3::contactPage::TextBlock_Loaded_3(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printaddemail(i));//显示email

}


void uwp课设3::contactPage::TextBlock_Loaded_4(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printaddtype(i));//显示type
}


void uwp课设3::contactPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//返回addressPage
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(AddressPage::typeid));//跳转callingpage

}
