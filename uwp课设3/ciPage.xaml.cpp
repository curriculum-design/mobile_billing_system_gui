﻿//
// ciPage.xaml.cpp
// ciPage 类的实现
//

#include "pch.h"
#include "ciPage.xaml.h"
#include "CallPage.xaml.h"
#include "SMSPage.xaml.h"
#include "AddressPage.xaml.h"
#include "CostMapPage.xaml.h"
#include "addressperfectPage.xaml.h"

using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

ciPage::ciPage()
{
	InitializeComponent();
}


void uwp课设3::ciPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//打电话按钮
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(CallPage::typeid));
}


void uwp课设3::ciPage::Button_Click_1(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//发短信按钮
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(SMSPage::typeid));
}


void uwp课设3::ciPage::Button_Click_2(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//通讯录按钮
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(AddressPage::typeid));

}


void uwp课设3::ciPage::Button_Click_3(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//账单按钮
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(CostMapPage::typeid));

}


void uwp课设3::ciPage::Button_Click_4(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//跳转添加联系人页面
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(addressperfectPage::typeid));

}
