﻿//
// addressperfectPage.xaml.cpp
// addressperfectPage 类的实现
//

#include "pch.h"
#include "addressperfectPage.xaml.h"
#include "data.h"
#include "help.h"
#include "ciPage.xaml.h"
using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板
namespace _addressperfectPage
{
	String^ no;
	String^ name;
	String^ unit;
	String^ num;
	String^ email;
	String^ type;
}
addressperfectPage::addressperfectPage()
{
	InitializeComponent();
	_addressperfectPage::no = "";
	_addressperfectPage::name = "";
	_addressperfectPage::unit = "";
	_addressperfectPage::num = "";
	_addressperfectPage::email = "";
	_addressperfectPage::type = "";
}



void uwp课设3::addressperfectPage::TextBox_TextChanged_1(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_addressperfectPage::no = tb->Text;
}


void uwp课设3::addressperfectPage::TextBox_TextChanged_2(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_addressperfectPage::name = tb->Text;
}


void uwp课设3::addressperfectPage::TextBox_TextChanged_3(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_addressperfectPage::unit = tb->Text;
}


void uwp课设3::addressperfectPage::TextBox_TextChanged_4(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_addressperfectPage::num = tb->Text;
}


void uwp课设3::addressperfectPage::TextBox_TextChanged_5(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_addressperfectPage::email = tb->Text;
}


void uwp课设3::addressperfectPage::TextBox_TextChanged_6(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_addressperfectPage::type = tb->Text;
}


void uwp课设3::addressperfectPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//提交联系人按钮
{
	address temp(help::toStdStr(_addressperfectPage::no), help::toStdStr(_addressperfectPage::name), help::toStdStr(_addressperfectPage::unit), help::toStdStr(_addressperfectPage::num), help::toStdStr(_addressperfectPage::email), help::toStdStr(_addressperfectPage::type));
	data::info->addressinitialize(temp);//压入一个temp
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(ciPage::typeid));

}
