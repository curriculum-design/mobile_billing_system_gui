#pragma once
#include<stdlib.h>
#include<vector>
#include <string>
#include<windows.h>
#include<Mmsystem.h>
#include<cstdio>
#include <sstream>
#include<cstring>
#pragma comment(lib,"winmm.lib")
using namespace std;

class message
{
private:
	string Recipientnum;//收信人
	string messagecontent;//短信内容
	static int count;//用来记录共计发短信条数
public:
	message(string, string);
	string printRecipientnum();
	string printmessagecontent();
	//static int printcount();
};

class address
{
private:
	string no;
	string name;
	string unit;
	string num;
	string email;
	string type;
public:
	friend class information;
	address(string no_,string name_, string unit_, string num_, string email_, string type_);
	string numprint();
	string nameprint();
};
class cost
{
private:
	string standard;
	double calltime;
	int messagenum;
	int traffic;
	int costsum;
	double callbill;
	double messagebill;
public:
	friend class information;
	cost();
	void getinmessagenum();
};


class information
{
private:
	string num;
	string name;
	string people;
	vector<address>add;
	vector<message>sms;
	cost objCost;
public:
	information(string num_, string name_/*, string people_*/) :objCost(), num(num_), name(name_)/*, people(people_)*/ {}//cost已被创建，值赋为0
	int messagenum;
	//void call(string);
	string printmessagenum();
	//void sendmessage(string, string);
	void getincalltime(double);
	
	void getsms(string, string);
	void addressperfect(string,string, string, string, string, string);
	void addressinitialize(address);//用来初始化information中的vector<address>add
	string printtraffic();
	string printcalltime();//用来返回information中objCost中的calltime
	vector<address> printaddress();
	int searchadd(string);
	string callcost();
	string messagecost();
	string sumcost();
	string smslogprint();//输出短信记录
	string printaddno(int);
	string printaddname(int);
	string printaddunit(int);
	string printaddnum(int);
	string printaddemail(int);
	string printaddtype(int);
};
