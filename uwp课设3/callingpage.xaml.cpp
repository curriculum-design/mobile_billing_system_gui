﻿//
// callingpage.xaml.cpp
// callingpage 类的实现
//

#include "pch.h"
#include "callingpage.xaml.h"
#include "data.h"
#include "help.h"
#include<windows.h>
#include<mmsystem.h> //windows中与多媒体有关的大多数接口
#include<ctime>
#include<ciPage.xaml.h>
#pragma commen(lib,"WINMM.LIB")
clock_t start, endtime;
using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

double time_;//用来存放时间
callingpage::callingpage()
{
	InitializeComponent();
}

void uwp课设3::callingpage::TextBlock_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//进入callingPage时触发
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::number);//显示number
	start = clock();//开始计时
}

namespace _callingpage
{
	MediaElement^ me;
}


void uwp课设3::callingpage::MediaElement_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//播放媒体控件在在载入界面时触发
{
	_callingpage::me = (MediaElement^)sender;
}

void uwp课设3::callingpage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//按动按钮挂断时触发
{
	_callingpage::me->Stop();//挂断时停止播放
	endtime = clock();//结束
	time_ = (endtime - start)*0.0000167;
	data::info->getincalltime(time_);//调用getincalltime，更新通话时间
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(ciPage::typeid));
}



