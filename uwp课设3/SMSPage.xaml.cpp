﻿//
// SMSPage.xaml.cpp
// SMSPage 类的实现
//

#include "pch.h"
#include "SMSPage.xaml.h"
#include "data.h"
#include "help.h"
#include "ciPage.xaml.h"
using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板
namespace _SMSPage
{
	String ^num;//
	String ^SMScontent;//短信内容
}
SMSPage::SMSPage()
{
	InitializeComponent();
	_SMSPage::num = "";
	_SMSPage::SMScontent = "";
}


void uwp课设3::SMSPage::TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^tb = (TextBox^)sender;
	_SMSPage::num = tb->Text;
}


void uwp课设3::SMSPage::TextBox_TextChanged_1(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^tb = (TextBox^)sender;
	_SMSPage::SMScontent = tb->Text;
}


void uwp课设3::SMSPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{  
	data::info->getsms(help::toStdStr(_SMSPage::num), help::toStdStr(_SMSPage::SMScontent));//向information的message（短信）类中压入一条短信，objCost短信数+1
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(ciPage::typeid));//跳转回到ciPage
}
