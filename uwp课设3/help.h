#pragma once
#include "pch.h"
#include <string>

class help
{
public:
	

	static std::string toStdStr(Platform::String^ ms)//向std转化
	{
		std::wstring w_str(ms->Begin());
		return std::string(w_str.begin(), w_str.end());
	}

	static Platform::String^ toPlaStr(const std::string & input)//向UWP转化
	{
		std::wstring w_str = std::wstring(input.begin(), input.end());
		const wchar_t* w_chars = w_str.c_str();
		return (ref new Platform::String(w_chars));
	}
};