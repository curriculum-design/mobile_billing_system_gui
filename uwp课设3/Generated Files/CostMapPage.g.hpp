﻿//------------------------------------------------------------------------------
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//------------------------------------------------------------------------------
#include "pch.h"

#if defined _DEBUG && !defined DISABLE_XAML_GENERATED_BINDING_DEBUG_OUTPUT
extern "C" __declspec(dllimport) int __stdcall IsDebuggerPresent();
#endif

#include "CostMapPage.xaml.h"

void ::uwp课设3::CostMapPage::InitializeComponent()
{
    if (_contentLoaded)
    {
        return;
    }
    _contentLoaded = true;
    ::Windows::Foundation::Uri^ resourceLocator = ref new ::Windows::Foundation::Uri(L"ms-appx:///CostMapPage.xaml");
    ::Windows::UI::Xaml::Application::LoadComponent(this, resourceLocator, ::Windows::UI::Xaml::Controls::Primitives::ComponentResourceLocation::Application);
}

void ::uwp课设3::CostMapPage::Connect(int __connectionId, ::Platform::Object^ __target)
{
    switch (__connectionId)
    {
        case 1:
            {
                ::Windows::UI::Xaml::Controls::TextBlock^ element1 = safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(element1))->Loaded += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::TextBlock_Loaded);
            }
            break;
        case 2:
            {
                ::Windows::UI::Xaml::Controls::TextBlock^ element2 = safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(element2))->Loaded += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::TextBlock_Loaded_1);
            }
            break;
        case 3:
            {
                ::Windows::UI::Xaml::Controls::TextBlock^ element3 = safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(element3))->Loaded += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::TextBlock_Loaded_2);
            }
            break;
        case 4:
            {
                ::Windows::UI::Xaml::Controls::TextBlock^ element4 = safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(element4))->Loaded += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::TextBlock_Loaded_3);
            }
            break;
        case 5:
            {
                ::Windows::UI::Xaml::Controls::TextBlock^ element5 = safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(element5))->Loaded += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::TextBlock_Loaded_4);
            }
            break;
        case 6:
            {
                ::Windows::UI::Xaml::Controls::Button^ element6 = safe_cast<::Windows::UI::Xaml::Controls::Button^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::Button^>(element6))->Click += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::Button_Click);
            }
            break;
        case 7:
            {
                ::Windows::UI::Xaml::Controls::Button^ element7 = safe_cast<::Windows::UI::Xaml::Controls::Button^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::Button^>(element7))->Click += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::Button_Click_1);
            }
            break;
        case 8:
            {
                ::Windows::UI::Xaml::Controls::TextBlock^ element8 = safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(__target);
                (safe_cast<::Windows::UI::Xaml::Controls::TextBlock^>(element8))->Loaded += ref new ::Windows::UI::Xaml::RoutedEventHandler(this, (void (::uwp课设3::CostMapPage::*)
                    (::Platform::Object^, ::Windows::UI::Xaml::RoutedEventArgs^))&CostMapPage::TextBlock_Loaded_5);
            }
            break;
    }
    _contentLoaded = true;
}

::Windows::UI::Xaml::Markup::IComponentConnector^ ::uwp课设3::CostMapPage::GetBindingConnector(int __connectionId, ::Platform::Object^ __target)
{
    __connectionId;         // unreferenced
    __target;               // unreferenced
    return nullptr;
}


