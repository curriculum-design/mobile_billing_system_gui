﻿//
// CallPage.xaml.cpp
// CallPage 类的实现
//

#include "pch.h"
#include<string>
#include "CallPage.xaml.h"
#include "msg.h"
#include "help.h"
#include "data.h"
#include "callingpage.xaml.h"

using namespace uwp课设3;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

string data::number;

CallPage::CallPage()
{
	InitializeComponent();
	data::number = "";//包在data头文件中，用来存放拨打的号码，在callingpage也可访问number
}

void uwp课设3::CallPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//按钮1
{
	data::number += "1";
}
void uwp课设3::CallPage::Button_Click_2(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//按钮2
{
	data::number += "2";
}
void uwp课设3::CallPage::Button_Click_1(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//按钮3
{
	data::number += "3";
}

void uwp课设3::CallPage::Button_Click_3(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//按钮4
{
	data::number += "4";
}

void uwp课设3::CallPage::Button_Click_4(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//5
{
	data::number += "5";
}

void uwp课设3::CallPage::Button_Click_5(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//6
{
	data::number += "6";
}

void uwp课设3::CallPage::Button_Click_6(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//7
{
	data::number += "7";
}

void uwp课设3::CallPage::Button_Click_7(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//8
{
	data::number += "8";
}


void uwp课设3::CallPage::Button_Click_8(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//9
{
	data::number += "9";
}

void uwp课设3::CallPage::Button_Click_10(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//0
{
	data::number += "0";
}

void uwp课设3::CallPage::Button_Click_9(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//打电话button
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(callingpage::typeid));//跳转callingpage
}
