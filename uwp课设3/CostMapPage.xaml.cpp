﻿//
// CostMapPage.xaml.cpp
// CostMapPage 类的实现
//

#include "pch.h"
#include "CostMapPage.xaml.h"
#include "data.h"
#include "help.h"
#include "ciPage.xaml.h"
#include "SMSlogPage.xaml.h"
using namespace std;

using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=234238 上介绍了“空白页”项模板

CostMapPage::CostMapPage()
{
	InitializeComponent();
}


void uwp课设3::CostMapPage::TextBlock_Loaded(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printcalltime());//显示calltime
}


void uwp课设3::CostMapPage::TextBlock_Loaded_1(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printmessagenum());//显示messagenum
}


void uwp课设3::CostMapPage::TextBlock_Loaded_2(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//话费
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->callcost());//显示callcost
}


void uwp课设3::CostMapPage::TextBlock_Loaded_3(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//短信费用
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->messagecost());//显示messagecost
}


void uwp课设3::CostMapPage::TextBlock_Loaded_4(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//总费用
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->sumcost());//显示sumcost

}


void uwp课设3::CostMapPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//返回至ciPage
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(ciPage::typeid));
}


void uwp课设3::CostMapPage::Button_Click_1(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//跳转SMSlogPage
{
	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(SMSlogPage::typeid));
}


void uwp课设3::CostMapPage::TextBlock_Loaded_5(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//流量费
{
	((TextBlock^)sender)->Text = help::toPlaStr(data::info->printtraffic());//显示traffic

}
