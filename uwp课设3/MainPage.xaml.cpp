﻿//
// MainPage.xaml.cpp
// MainPage 类的实现。
//

#include "pch.h"
#include "MainPage.xaml.h"
#include "ciPage.xaml.h"
#include "msg.h"
#include "help.h"
#include "data.h"

using namespace uwp课设3;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x804 上介绍了“空白页”项模板

namespace _MainPage
{
	String^ name;
	String^ number;
	String^ people;
}

MainPage::MainPage()
{
	InitializeComponent();
	_MainPage::name = "";
	_MainPage::number = "";

}

void uwp课设3::MainPage::TextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_MainPage::name = tb->Text;
}

void uwp课设3::MainPage::TextBox_TextChanged_1(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e)
{
	TextBox^ tb = (TextBox^)sender;
	_MainPage::number = tb->Text;
}


information* data::info;

void uwp课设3::MainPage::Button_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e)//按按钮将文本框信息存入textContent
{
	data::info= new information(help::toStdStr(_MainPage::number), help::toStdStr(_MainPage::name));//初始化information
	address a1("1", "Sam", "a", "15465421587", "2254625484@qq.com", "friend");
	address a2("2", "Tony", "b", "19415161587", "8664625484@qq.com", "friend");
	data::info->addressinitialize(a1);
	data::info->addressinitialize(a2);

	this->Frame->Navigate(Windows::UI::Xaml::Interop::TypeName(ciPage::typeid));//跳转ciPage页面
}



