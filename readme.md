手机计费系统
=============
功能分析
---------
1. 计算通话费用、短信费用、流量费、总费用
2. 定时更新实时费用对象
3. 模拟通话、发短信记录，显示通讯录信息、有电话拨通的声音

功能设计
----------
* 输入个人信息
* 拨打电话
* 发送短信
* 查看通讯录
* 添加联系人
* 查看账单
* 查看短信记录

类设计与类结构
-------------
### 类设计
* 信息类(information)
* 费用类(cost)
* 通讯录类(address)
* 短信类(message)
### 类结构
![p2](mdimg/p2.png)

功能模块设计
------------
### 辅助模块
* 头文件`help.h`用来转化`string`类型，`static std::string toStdStr`函数为`Platform::String`向`std::string`转化，`static Platform::String^ toPlaStr`反之
* 头文件`data.h`用来存取数据——`information`的对象指针和电话号码`number`
### information模块：
* 初始化函数
* printmessagenum()
* 数据访问器`getincalltime(double time)`、`getsms(string, string)`
* 完善通讯录函数`addressperfect()`
* 通讯录联系人初始化函数`addressinitialize(address)`
* 返回数据成员函数`printtraffic()`、`printcalltime()`、`printaddress()`
* 计算费用函数`callcost()`、`messagecost()`、`sumcost()`

### MainPage页面
该页面含有两个TextBox，输入字符串时触发`MainPage::TextBox_TextChanged`函数，将文本内容分别赋值给`_MainPage::name`和`_MainPage::number`。定义一个`information`类对象指针`info`，在按动Button时触发`MainPage::Button_Click`函数，调用`information`的构造函数初始化`info`，初始化两个`address`对象`a1`、`a2`，通过`data::info->addressinitialize`函数压入vector中，并且跳转到`ciPage`页面

### ciPage页面
该页面含有5个button，分别跳转`CallPage`、`SMSPage`、`AddressPage`、`CostMapPage`、`addressperfectPage`页面

### CostMapPage页面
该页面含有6个TextBlock：
1. 载入时用`help::toPlaStr()`函数将`data::info->printcalltime()`(返回通话时间的函数)从C++ string向UWP转化，给`(TextBlock^)sender)->Text`赋值，并在其中显示
2. 同1，调用`printmessagenum()`函数
3. 使用`callcost`函数计算通话费用，先将通话时间向上取整，再乘通话单价，转化成`string`类型返回，在TextBlock中显示。
4. 同3，调用`messagecost`显示短信费用
5. 先用`Type stringToNum`(用来将`string`转化成`Type`)模板函数(令`Type`等于`double`型)，将`messagecost()`和`callcost()`转化成`double`类型相加后再将结果转化成`string`，用`help::toPlaStr`转化后在TextBlock中显示
6. 调用`printtraffic`函数返回流量值显示流量费用

该页面含有两个button：
* 返回`ciPage`
* 跳转`SMSlogPage`(短信记录页面)

### SMSlogPage页面
该页面有一个TextBlock，载入时调用`smslogprint`函数，循环`sms.size()`次(`sms`为`info`中的短信`vector`成员)，每次循环将循环下标对应的`sms`的`Recipientnum`和`messagecontent`成员通过`sms[i].printRecipientnum`和`sms[i].printmessagecontent`函数压入`str`字符串中并将其返回显示。

该页面有一个button用来跳转`ciPage`

### addressperfectPage(通讯录完善页面)
该页面有6个TextBlock，内容发生改变时触发`TextBox_TextChanged`函数，将输入的6条信息分别存入`no`、`name`、`unit`、`num`、`email`、`type`，用`help::toStdStr`转化后给`address`类对象`temp`成员初始化，使用`data::info->addressinitialize`函数压入`vector`中

该页面有1个button用来返回`ciPage`

### AddressPage(通讯录页面)
该页面有一个列表，先使用`appendItem`函数用来向纵向堆栈中添加条目，通过for循环(截止条件为通讯录中联系人个数)动态生成`n`个条目，列表内容通过`data::info->printaddname(i)`显示联系人姓名。

点击每一个条目时，将点击的条目的内容存入`content`，用`data::info->searchadd`函数进行搜索，返回值为选中`add`的下标赋值给`_contactPage::selectSub`，并且进入`contactPage`

也可以在TextBox中搜索联系人姓名，姓名存入`_AddressPage::searchname`，同样使用`data::info->searchadd`搜索获得`add`下标赋值给`_contactPage::selectSub`，按动button2进入`contactPage`

### contactPage(名片页)：
通过`_contactPage::selectSub`分别调用`info`的6个`print`函数，获取该联系人的信息在6个TextBlock中展示

按动button返回`AddressPage`

### cost类的结构与功能设计
![p3](mdimg/p3.png)
